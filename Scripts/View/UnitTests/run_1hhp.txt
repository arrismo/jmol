python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/colorChainA.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/colorBackboneAllChains.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/highlightHydrogens.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/showBackbone.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/showBackboneAllModels.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/showBackboneModel1.spt

python ../view_with_jmol.py ../../../Data/Cif/1hhp.cif ../JMolScripts/viewModel1.spt


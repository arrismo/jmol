python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorChainA.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorBackboneAllChains.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/highlightHydrogens.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/showBackbone.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/showBackboneAllModels.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/showBackboneModel1.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/viewModel1.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorProtein.spt
python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorDna.spt
python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorRna.spt

python ../view_with_jmol.py ../../../Data/Cif/1cwp.cif ../JMolScripts/colorMolecules.spt

load ../../../Data/Cif/1cwp.cif FILTER biomolecule 1;


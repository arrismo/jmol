'''
    File:           view_with_jmol.py 
    Author:         Ileana Streinu
    Date created:   2019: Oct 7
    Updates: 	    2019: Oct 7
    Last modified:  2019: Oct 7
    Python Version: 3

    Description:    Script to open a cif file with JMol and run a JMol script for viewing it 
    Usage:  python view_with_jmol.py input_cif_file_path input_spt_file_path
'''


import sys,os

PATH_TO_JMOL="../../../External/jmol/Jmol.jar"    

#------------------------------------------------------------    view_file_with_JMol
def view_file_with_JMol(input_cif_file_path,jmol_script_file_path):
    print("======================================================")
    cmd= 'java -jar ' + PATH_TO_JMOL + " " + input_cif_file_path + " -s " + jmol_script_file_path
    print("command = "+cmd)
    print("======================================================")
    os.system(cmd)
    
    
    
#----------------   main -------------------------------------    
if __name__ == "__main__":
    argv=sys.argv[1:]

    if len(argv) < 2:
        print('usage: python %s input_cif_file_path jmol_script_file_path' % os.path.basename(__file__)) 
        sys.exit()

    input_cif_file_path = argv[0]    
    jmol_script_file_path = argv[1]
    
    if os.path.isfile(input_cif_file_path) and os.path.isfile(jmol_script_file_path):
        view_file_with_JMol(input_cif_file_path,jmol_script_file_path)
    else:
        print("ERROR: could not find input file(s)")